package cloud.simple.hello;

import org.springframework.boot.actuate.health.HealthIndicator;

import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

@Component

public class MyHealth implements HealthIndicator { 
	volatile static boolean b = false;

    @Override

    public Health health() {

        int errorCode = check(); // perform some specific health check

        if (errorCode != 0) {

        	return Health.down().withDetail("Error Code", errorCode).build();

        }else{
        	return Health.up().withDetail("hello", "world").build();
        }

    }
    private int check(){
    	b = !b;
    	if(b)
    		return 123;
    	else
    		return 0;
    }

}